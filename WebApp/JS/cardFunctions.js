﻿//Toggle open and close the information tables
$(function () {
    $('.tableHeader').click(function () {
        $(this).nextUntil('tr.tableHeader').slideToggle(1);
    });
});

$(function () {
    $('.accordion').click(function () {
        this.classList.toggle('active');
        $(this).nextUntil('div.tableStuff').slideToggle(1);
    });
});

var Month = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
var TotalConcessions2016 = [519.75, 96.70, 90.36, 446.18, 72.48, 196.60, 952.81, 247.26, 133.18, 947.76, 161.92, 117.16];
var TotalConcessions2015 = [0, 0, 0, 579.43, 142.35, 198.06, 596.32, 173.65, 162.54, 533.45, 136.50, 130.75];

var ctx = document.getElementById("myChart");
var mixedChart = new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
            label: '2016',
            data: TotalConcessions2016,
            pointBackgroundColor: "#004A68",
            backgroundColor: "#004A68",
            borderColor: "#004A68",
            fill: false,
            type: 'line'
        }, {
            label: '2015',
            data: TotalConcessions2015,
            pointBackgroundColor: "#853245",
            backgroundColor: "#853245",
            borderColor: "#853245",
            fill: false,
            type: 'line'
        }],
        labels: Month
    },
    options: {
        title: {
            display: true,
            text: "Monthly Earnings (thousands)"
        }
    }
});


// Our labels along the x-axis
var yearQualifier = ["2017", "2018"];
// For drawing the lines
var ytdData = [151, 175];

var ctx = document.getElementById("myChart2");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: yearQualifier,
        datasets: [
            {
                label: "YTD",
                data: ytdData,
                backgroundColor: "#004A68"
            },
            {
                label: "Annualized YTD",
                data: [368, 388],
                backgroundColor: "#853245"
            }
        ]
    },
    options: {
        title: {
            display: true,
            text: "Yearly Earnings (thousands)"
        }
    }
});

var accountTypes = ["Pershing", "FCCS", "Other"];
var accountCount = [10, 301, 189];

var ctx = document.getElementById("myChart3");
var myPieChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: accountTypes,
        datasets: [
            {
                data: accountCount,
                backgroundColor: ["#004A68", "#853245", "#D4AF3B"]
            }
        ]
    },
    options: {
        title: {
            display: true,
            text: "Account Count"
        }
    }
});
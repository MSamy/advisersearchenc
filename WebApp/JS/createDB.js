


//trigger
//import * as data1 from 'Advisers.js';
var repList = [];
var username = "Mohamed";
var options = { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 };
var searchIndex = "";

function loaddata ()
{

    var a = $.Deferred();
    //var r = $.Deferred();
    document.getElementById("status").textContent="Loading ....";

    var key  = prompt("Please enter your code", "");

    $.getJSON('WebApp/JS/AdvisorEncrypted10.js', function (data) {


        db.transaction(function (tx) {
            // tx.executeSql('Drop Table Adviser');

            tx.executeSql('DELETE FROM Adviser');


            data.map((row) =>{

                try {


                    var decryptedjson = CryptoJS.AES.decrypt(row.data, key, options);
                    var plaintext = decryptedjson.toString(CryptoJS.enc.Utf8);


                    //decryptedjson.toString(CryptoJS.enc.Utf8);
                    //var decryptedjsondecoded =  plaintext.toString(CryptoJS.enc.Utf8);
                    var decryptedrow = JSON.parse(plaintext);

                    searchIndex = decryptedrow.Name.FirstName + " " + decryptedrow.Name.LastName + " - " + decryptedrow.PrimaryCompensationCode + " - " + decryptedrow.Contactid;
                    repList.push(searchIndex);
                    // populate.replaceproductionnumbers(decryptedrow);
                    var parsedData = JSON.stringify(decryptedrow);
                    parsedData = "[" + parsedData + "]";
                    tx.executeSql('INSERT INTO Adviser (repcode, FirstName, Lastname, SearchIndex, DATA,CRMContactID) VALUES (?,?,?,?,?,?)', [decryptedrow.PrimaryCompensationCode, decryptedrow.Name.FirstName, decryptedrow.Name.LastName, searchIndex, parsedData, decryptedrow.Contactid]);
                }
                catch (e) {
                    console.log(e.toString());

                }

            });
            // endmap
            /*for (var i = 0; i < data.length; i++) {
                var searchIndex = data[i].Name.FirstName + " " + data[i].Name.MiddleName + " " + data[i].Name.LastName + " - " + data[i].PrimaryCompensationCode;
                repList.push(searchIndex);
                populate.replaceproductionnumbers(data[i]);
                var parsedData = JSON.stringify(data[i]);
                parsedData = "[" + parsedData + "]";


                var    key = "d5b3e010c1883e57f6ed0a87595308c7";
                console.log("Create DB v2 :" + key);
                //var encryptedData = parsedData;
                // var encryptedData = CryptoJS.AES.encrypt(parsedData, key);

                tx.executeSql('INSERT INTO Adviser (repcode, FirstName, Lastname, SearchIndex, DATA) VALUES (?,?,?,?,?)', [data[i].PrimaryCompensationCode, data[i].Name.FirstName, data[i].Name.LastName, searchIndex, parsedData]);

            } */
            a.resolve();
        });

        console.log('database loaded');
    });
   // r.resolve();
    return $.Deferred(function (def) {
        $.when(a).done(function () {
            def.resolve();
        });
    });

}
$().ready(function () {



    db = openDatabase('Adviserdb', '1.0', 'my first database', 2 * 1024 * 1024);

    var flagloaded = false;
    // check if data already loaded
    db.transaction(function (tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS Adviser (repcode , FirstName, LastName, SearchIndex,CRMContactID, DATA,NOTES)');
        tx.executeSql('Select * from Adviser', [], function (tx, results) {
            if (results.rows.length > 0) {
                flagloaded = true;
                //load index
                for (var i = 0; i < results.rows.length; i++) {

                    repList.push(results.rows.item(i).SearchIndex);

                }
            }
            else
            {

               // var status = $("#status");


               // status.val("Loading....");
                loaddata().done(function(){
                    document.getElementById("status").textContent="Done";

                });
               // status.val("Done");

            }

        });
    });




   // document.getElementById("offlineSearchBTN").addEventListener("click", searchclick);
    document.getElementById("txtNotes").addEventListener("click", scrolltextnotes);
   //document.getElementById("btnexportNotes").addEventListener("click", exportNotes);
    var input = document.getElementById("IFirstName");
    new Awesomplete(input, { list: repList });
    var btnsave = document.getElementById("btnSaveNotes");
    btnsave.innerText = "save";

    $('.result').hide();
    $('.resultheader').hide();
    $('.additional-info').hide();

    exportNotes();
});

function exportNotes() {
    var mailbody = "mailto:myself@cir2.com?subject='Ignite notes'&body=" ;
    db.transaction(function (tx) {
    tx.executeSql('Select * from Adviser where NOTES <> ""', [], function (tx, results) {
        if (results.rows.length > 0) {
            flagloaded = true;
            //load index
            for (var i = 0; i < results.rows.length; i++) {

                //repList.push(results.rows.item(i).SearchIndex);
                mailbody = mailbody  +  results.rows.item(i).SearchIndex +  "||" +  results.rows.item(i).NOTES + "%0D%0A";


            }
        }
        else
        {
           // alert("No notes found");
        }
       // var b = $('Exportmynotes');

       // b.href =  mailbody;

       var b = document.getElementById("Exportmynotes");
        b.href =  mailbody;
        //btnexport.prop("href", mailbody) ;
    });
    });
}
function scrolltextnotes() {

        $("html, body").animate({ scrollTop: $("#txtNotes").offset().top }, 300);
        return true;


}







function searchclick() {

    //document.getElementById("offlineSearchBTN").addEventListener("click", searchclick);
    var txtnotes = $("#txtNotes") ;
    txtnotes.val("");


    var alertExists = document.getElementById("errorAlert");
    if (alertExists) {
        document.getElementById("errorAlert").remove();
    }

    var userInput = $('#IFirstName').val();
    var userInputSplit = userInput.split(" ");
    var firstName = userInputSplit[0];
    var lastName = userInputSplit[1];
    var repCode = userInputSplit[3];
    var CRMContactID = userInputSplit[5];

    if ((firstName === "") || (firstName === undefined)) { firstName = "%"; }
    if ((lastName === "") || (lastName === undefined)) { lastName = "%"; }
    if ((repCode === "") || (repCode === undefined)) { repCode = "%"; }

    console.log(firstName + " - " + lastName + " - " + repCode);
    //document.getElementById('CIR-Rep-Info').innerHTML = "";

    db = openDatabase('Adviserdb', '1.0', 'my first database', 2 * 1024 * 1024);

    $('#IFirstName').val("");
    $('#IRepCode').val("");
    $('.data').text("");
    //$("#txtNotes").val("");

    db.transaction(function (tx) {
        //'SELECT * FROM Adviser WHERE FirstName Like ? and LastName Like ?', [firstName, lastName] SearchIndex
        tx.executeSql('SELECT * FROM Adviser WHERE SearchIndex Like ?', [userInput], function (tx, results) {
            if (results.rows.length > 0) {
                var jsonResult;
                for (var i = 0; i < results.rows.length; i++) {

                    //var unencrypt  = CryptoJS.AES.decrypt(results.rows.item(i).DATA, key);
                    var unencrypt  = results.rows.item(i).DATA;
                    searchIndex = results.rows.item(i).SearchIndex;

                    var data = unencrypt;

                    jsonResult = JSON.parse(data);
                    //var decrypted = CryptoJS.AES.decrypt(results.rows.item(i).DATA, key);
                    var repIdentifier = jsonResult[0]['Name']['FirstName'] + jsonResult[0]['Name']['LastName'] + jsonResult[0]['PrimaryCompensationCode'];
                    //document.getElementById('CIR-Rep-Info').innerHTML = advisorCardFunctions.addUserInfo(jsonResult[0]) + advisorCardFunctions.addDrillDownInfo(jsonResult[0]);
                    $(".temphidden").show().removeClass(".temphidden");
                    populate.populateBasicInfo(jsonResult[0]);
                    $("#BasicInfo").show();
                    populate.populateBusinessAddress(jsonResult[0]);
                    populate.populateSupervision(jsonResult[0]);
                    populate.populateBranchInfo(jsonResult[0]);
                    populate.populateAccountInfo(jsonResult[0]);
                    populate.populateActiveRepProd(jsonResult[0]);
                    populate.populateBusinessInformation(jsonResult[0]);
                    populate.populateRecruitBusinessMix(jsonResult[0]);
                    populate.populateNotes( advisorCardFunctions.isNameUndefinedOrNull( results.rows.item(i).NOTES));


                     dropDownMenu();

                    var btnsave = document.getElementById("btnSaveNotes");
                    btnsave.innerText = "save";
                     $("#btnSaveNotes").click(function () {
                         exportNotes();
                         advisorCardFunctions.saveNotes();

                     });

                    $("#selectorrow").removeClass(".display");
                    $('.resultheader').removeClass(".display");
                    $('.result').removeClass(".display");
                    $("#selectorrow").show();
                    $('.additional-info').show();
                    //$("#ActiveRepProductionOption").click();

                    //hide non recruit options
                    if (jsonResult[0]['PrimaryCompensationCode'] === "Recruit"){
                        $(".reponly").hide();
                        $(".recruitonly").show();
                        $("#BusinessAddressOption").click();

                    }
                    else
                    {
                        $(".recruitonly").hide();
                        $(".reponly").show();
                        $("#ActiveRepProductionOption").click();
                    }

                    //$('.selector').show();

                   // $("#BasicInfo").addClass("selected");
                    //$("#selectorrow").addClass("selected");
                    //$("#selector").addClass("selected");
                    //$("#testdiv").addClass("selected");


                }
            }
        }, null);
    });


}

function transError(t, e) {
    console.log(t);
    console.log(e);
    var btn =document.getElementById("btnSaveNotes");
    btn.innerText = "error";
    console.error("Error occured ! Code:" + e.code + " Message : " + e.message);
}

function transSuccess(t, r) {
    console.info("Transaction completed Successfully!");
    var btn =document.getElementById("btnSaveNotes");
    btn.innerText = "saved";
    //$("#btnSaveNotes").value = "saved";
    exportNotes();
    console.log(t);
    console.log(r);
}



var aes = {
    encyrpt: function(data){

    },
    decrypt: function (data) {

    }
}

var populate ={



    replaceproductionnumbers: function(data){
        data["IndividualProduction"]["CRDNumber"] =  9;
        data["IndividualProduction"]["GDCCurrentYTD"] =  9;
        data["IndividualProduction"]["GDCCurrentYTDBase"] =  9;
        data["IndividualProduction"]["GDCCurrentYTDTerm"] =  9;
        data["IndividualProduction"]["GDCCurrentYTD -Term -Base"] =  9;
        data["IndividualProduction"]["CY -TktChgAnnl -Term"] =  9;
        data["IndividualProduction"]["CY -TktChgAnnl -Term -Base"] =  9;
        data["IndividualProduction"]["CY -TktChgYTD -Term"] =  9;
        data["IndividualProduction"]["CY -TktChgYTD -Term -Base"] =  9;
        data["IndividualProduction"]["CY -TktChrgClntAnnl"] =  9;
        data["IndividualProduction"]["CY -TktChrgClntAnnl -Base"] =  9;
        data["IndividualProduction"]["CY -TktChrgClntYTD"] =  9;
        data["IndividualProduction"]["CY -TktChrgClntYTD -Base"] =  9;
        data["IndividualProduction"]["GDCAnnualizedPriorYTD"] =  9;
        data["IndividualProduction"]["PriorYearTotal -Base"] =  9;
        data["IndividualProduction"]["PriorYearTotal -Term"] =  9;
        data["IndividualProduction"]["PriorYearTotal -Term -Base"] =  9;
        data["IndividualProduction"]["GDCPriorYTD"] =  9;
        data["IndividualProduction"]["PriorYTD -Base"] =  9;
        data["IndividualProduction"]["PriorYTD -Term"] =  9;
        data["IndividualProduction"]["PriorYTD -Term -Base"] =  9;
        data["IndividualProduction"]["OtherAccounts"] =  9;
        data["IndividualProduction"]["PershingAccounts"] =  9;
        data["IndividualProduction"]["FCCSAccounts"] =  9;
        data["IndividualProduction"]["OtherAccountCount"] =  9;
        data["IndividualProduction"]["PershingAccountCount"] =  9;
        data["IndividualProduction"]["FCCSAccountCount"] =  9;

        data["IndividualProduction"]["GDCPercentGrowthYTD"] =  9;
        data["IndividualProduction"]["GDCAnnualizedCurrentYTD"] =  9;
        data["IndividualProduction"]["GDCAnnualizedCurrentYTDBase"] =  9;
        data["IndividualProduction"]["GDCAnnualizedCurrentYTDTerm"] =  9;
        data["IndividualProduction"]["GDCAnnualizedCurrentYTDTermBase"] =  9;

    },
    populateBasicInfo: function (data){
        var img = advisorCardFunctions.isNameUndefinedOrNull(advisorCardFunctions.hexToBase64(data['CRMContactImage'] ));
        var imageempty = 'MHhGRkQ4RkZFMDAwMTA0QTQ2NDk0NjAwMDEwMTAxMDA2MDAwNjAwMDAwRkZEQjAwNDMwMDA4MDYwNjA3MDYwNTA4MDcwNzA3MDkwOTA4MEEwQzE0MEQwQzBCMEIwQzE5MTIxMzBGMTQxRDFBMUYxRTFEMUExQzFDMjAyNDJFMjcyMDIyMkMyMzFDMUMyODM3MjkyQzMwMzEzNDM0MzQxRjI3MzkzRDM4MzIzQzJFMzMzNDMyRkZEQjAwNDMwMTA5MDkwOTBDMEIwQzE4MEQwRDE4MzIyMTFDMjEzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyMzIzMjMyRkZDMDAwMTEwODAwOTAwMDkwMDMwMTIyMDAwMjExMDEwMzExMDFGRkM0MDAxRjAwMDAwMTA1MDEwMTAxMDEwMTAxMDAwMDAwMDAwMDAwMDAwMDAxMDIwMzA0MDUwNjA3MDgwOTBBMEJGRkM0MDBCNTEwMDAwMjAxMDMwMzAyMDQwMzA1MDUwNDA0MDAwMDAxN0QwMTAyMDMwMDA0MTEwNTEyMjEzMTQxMDYxMzUxNjEwNzIyNzExNDMyODE5MUExMDgyMzQyQjFDMTE1NTJEMUYwMjQzMzYyNzI4MjA5MEExNjE3MTgxOTFBMjUyNjI3MjgyOTJBMzQzNTM2MzczODM5M0E0MzQ0NDU0NjQ3NDg0OTRBNTM1NDU1NTY1NzU4NTk1QTYzNjQ2NTY2Njc2ODY5NkE3Mzc0NzU3Njc3Nzg3OTdBODM4NDg1ODY4Nzg4ODk4QTkyOTM5NDk1OTY5Nzk4OTk5QUEyQTNBNEE1QTZBN0E4QTlBQUIyQjNCNEI1QjZCN0I4QjlCQUMyQzNDNEM1QzZDN0M4QzlDQUQyRDNENEQ1RDZEN0Q4RDlEQUUxRTJFM0U0RTVFNkU3RThFOUVBRjFGMkYzRjRGNUY2RjdGOEY5RkFGRkM0MDAxRjAxMDAwMzAxMDEwMTAxMDEwMTAxMDEwMTAwMDAwMDAwMDAwMDAxMDIwMzA0MDUwNjA3MDgwOTBBMEJGRkM0MDBCNTExMDAwMjAxMDIwNDA0MDMwNDA3MDUwNDA0MDAwMTAyNzcwMDAxMDIwMzExMDQwNTIxMzEwNjEyNDE1MTA3NjE3MTEzMjIzMjgxMDgxNDQyOTFBMUIxQzEwOTIzMzM1MkYwMTU2MjcyRDEwQTE2MjQzNEUxMjVGMTE3MTgxOTFBMjYyNzI4MjkyQTM1MzYzNzM4MzkzQTQzNDQ0NTQ2NDc0ODQ5NEE1MzU0NTU1NjU3NTg1OTVBNjM2NDY1NjY2NzY4Njk2QTczNzQ3NTc2Nzc3ODc5N0E4MjgzODQ4NTg2ODc4ODg5OEE5MjkzOTQ5NTk2OTc5ODk5OUFBMkEzQTRBNUE2QTdBOEE5QUFCMkIzQjRCNUI2QjdCOEI5QkFDMkMzQzRDNUM2QzdDOEM5Q0FEMkQzRDRENUQ2RDdEOEQ5REFFMkUzRTRFNUU2RTdFOEU5RUFGMkYzRjRGNUY2RjdGOEY5RkFGRkRBMDAwQzAzMDEwMDAyMTEwMzExMDAzRjAwRjdGQTI4QTI4MDBBMjhBMjgwMEEyOEEwRjRBMDA0MjQwMTkyNDAwM0Q2QjEyRjc1RjQ4Qzk0QjU1MEVDMzhERTdBN0UxNTU3NUJENTFBNTkwREI0MkM0NDZBNzBDNDdGMTFGNEZBNTYyRDAwNUE5QjUxQkM5QzlGMzJFMUM4M0Q4MUMwRkQyQUI4Nzc1N0RDMTk4MzBFRTBEMzY4QTAwRDhCNEQ3RTY4NTAyNEU5RTY4MUZDNTlDMzdGRjVFQjVBREI1QUIzQjg2MEJCOEM2QzdBNkYxOENGRTM1QzhEMTQwMURGODIwRDJENzNGQTBFQTJDNUZFQzkyOTI3OENDNjRGNUZBNTc0MTQwMDUxNDUxNDAwNTE0NTE0MDA1MTQ1MTQwMDUxNDUxNDAwNTU2RDQyNzM2RDYzMzRBM0VGMDVFM0VBNzhBQjM1OTNFMjE3REJBNkUzRkJDRTA1MDA3MkJENEU0RDE0NTE0MDA1MTQ1MTQwMDUxNDUxNDAxNkY0QzZEQkE5REI5RkY2QzBBRUQ2Qjg4RDNGRkU0MjM2RkYwMEY1RDE3RjlENzZGNDAwNTE0NTE0MDA1MTQ1MTQwMDUxNDUxNDAwNTE0NTE0MDA1NjM3ODkzRkUzQzIzRkZBRTlGRDBENkNENjQ3ODg5NEI2OUNBN0ZCQjIwRkU0NjgwMzk2QTI4QTI4MDBBMjhBMjgwMEEyOEEyODAxRjE0ODYyOTUyNDVFQThDMTg3RTE1REQ0NEZFNjQyOEY4QzZFNTA3MTVDMkM1MUI0RDMyNDRCRjc5RDgwMTVEREM2QTEyMzU0MUQxNDYyODAxRDQ1MTQ1MDAxNDUxNDUwMDE0NTE0NTAwMTQ1MTQ1MDAxNTFDRjBBNUM0MEQxNDgzMkFDMzA2QTRBMjgwMzg0Qjk4MUVEQUU2NDg1QzFDQTlDN0Q0N0FENDU1RDFGODhFREMzNDExREMwMUYzMkI2RDNGNEZGMDAzRkNFQjlDQTAwMjhBMjhBMDAyOEEyOTU1NEIzMDU1MTkyNEUwNTAwNkU3ODdFQzM3M0ZEQjI0MUMwRTEwN0JGNzM1RDFENDE2OTBGRDlFRDYyOEJGQjhBMDFBOUU4MDBBMjhBMjgwMEEyOEEyODAwQTI4QTI4MDBBMjhBMjgwMEEyOEEyODAyQjVFREIwQkFCMzkyMTNGQzQzOEZBRjZBRTIzMTgzODNENDcwNkJBRkJGRDYyREVDRjI5OTMyNEI4RkJBQkRCRUI1QzhCMUNCMTNEQzlDOUEwMDRBMjhBMjgwMEFCQkE0NDYyNEQ1NjAwN0EwM0JCMUY0MTlBQTU1MzVBNUMxQjVCQjhFNzAzM0I0RjRBMDBFRUE4QTgyREFFRTFCQjhDM0MyRkJCRDQ3NzFGNUE5RTgwMEEyOEEyODAwQTI4QTI4MDBBMjkxOTk1MTRCMzEwMTQwQzkyN0I1NjY0RkFGNTlDMjQ4NTY2OTRGRkIyM0ZBRDAwNkE1MTVDRTRCRTI1OTBGRkE5ODE1N0ZERTM5QUEzMzZCMTdEM0YwNjcyQTNEMTA2MjgwM0FDOUFFNjE4MTczMzQ4QTgzRENENjFERkVCRTE5NEM3NjgwRjNDMTcyMzE4RkE1NjBCMzMzQjZFNjYyNEZBOTM0OTQwMEE0OTJDNDkzOTI3QjlBNEEyOEEwMDI4QTI4QTAwMjhBMjhBMDBCMTY3NzcyRDk1QzJDQjExRTlENTczQzExNUQ3REE1RjQxNzkxMDc4OUM2NEY1NTNENDU3MTE0QTE4QTkwNTQ5MDQ3NDIwRDAwNzdGOUEyQjkwQjZENkVGMkRGODJFMjU1MUQ5RkZDN0FENkNEQjZCRjZCMzYxNjVDQzJERUZDOEZDRTgwMzVBOEE2NDcyQTRBQTFBMzc1NjVGNTA3MzRGQTAwQzlEN0VFM0M5QjFGMkMzNjFBNTZDN0UxREZGQTU3MkI1QjNFMjM5NzdERUM3MTgzRjcxMzNGODlBQzZBMDAyOEEyOEEwMDI4QTI4QTAwMjhBMjhBMDAyOEEyOEEwMDI4QTI4QTAwMjhBMjhBMDAyOEEyOEEwMDk2MEJBOUFENUMzQzJFNTRGN0M3N0FFQUY0QkQ0ODVGQzI3NzAwQjJBN0RFMDNGOUQ3MUY1QTdBMENCRTVFQTZBMDkwMDNBOTVGQzdCN0YyQTAwQURBOTRERTdFQTEzNDgwRTQ2RUMwM0VDMzhBQUI0NTE0MDA1MTQ1MTQwMDUxNDUxNDAwNTE0NTE0MDA1MTQ1MTQwMDUxNDUxNDAwNTE0NTE0MDA1MTQ1MTQwMDUzQTM3NjhFNDU3NTM4NjUzOTE0REEyODAwQTI4QTI4MDBBMjhBMjgwMEEyOEEyODAwQTI4QTI4MDBBMjhBMjgwMEEyOEEyODAwQTI4QTI4MDBBMjhBMjgwMEEyOEEyODAzRkZEOQ==';
        var imgstart = "MHhGRkQ4RkZFMDAwMTA0QTQ2NDk0NjAwMDEwMTAxMDA2MDAwNjAwMDAwRkZEQj";
        if (img === "" || img ==='-' || img === imageempty || img === 'AA==' || img.startsWith(imgstart,0) )
        {


            if (data['PersonalInformation']['Gender'] === "Male") {
                document.getElementById("repimage").src = "WebApp/Images/img_avatar.png";
            }
            if (data['PersonalInformation']['Gender'] === "Female"){
                document.getElementById("repimage").src = "WebApp/Images/img_avatar2.png";
            }
            else{
                document.getElementById("repimage").src = "WebApp/Images/Nuetral.jpg"
            }
        }
        else
        {

            document.getElementById("repimage").setAttribute("src", "data:image/png;base64, " + advisorCardFunctions.hexToBase64(data['CRMContactImage'] ));


        }

        document.getElementById("repname").innerText = data['Name']['FirstName'] + " " + data['Name']['MiddleName'] + " " +  data['Name']['LastName'];
        document.getElementById("repcode").innerText = data['PrimaryCompensationCode'];
        document.getElementById("years").innerText = advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['AdvisorSince']);
         populate.CAAPBadge(data);
         populate.getclubmembership(data);
        document.getElementById("RD").innerHTML = populate.getRegionalDirector(data);



    },
    populateBusinessAddress:function(data){
        document.getElementById("BranchAddress_Street_1").innerText =
            advisorCardFunctions.isUndefinedOrNull(data['BusinessLocation']['BranchAddress_Street_1']);
        document.getElementById("BranchAddress_Street_2").innerText =
            advisorCardFunctions.isUndefinedOrNull(data['BusinessLocation']['BranchAddress_Street_2']);
        document.getElementById("BranchCity").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessLocation']['BranchAddress_City']);
        document.getElementById("BranchState").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessLocation']['BranchAddress_State']);

    },

    //
    populateBranchInfo:function(data){
       document.getElementById("ParentOrganization").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['ParentOrganization']);
       document.getElementById("BranchManager").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['BranchManagerName']);
       document.getElementById("BranchMangerRepNumber").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['BranchManagerPrimaryCompensationCode']);
       document.getElementById("BranchNumber").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['PrimaryBranchNumber']);
       document.getElementById("BusinessUnitManager").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['BusinessUnitManager']);
       },

    populateAccountInfo: function(data){


        document.getElementById("numberofFCCS").innerText = advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['FCCSAccountCount']);
        document.getElementById("numberofPershing").innerText = advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['PershingAccountCount']);
        document.getElementById("numberofOther").innerText  = advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['OtherAccountCount']);
    },

    populateSupervision:function(data){


        document.getElementById("OSJSupervisior").innerText = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['OSJName']);

        document.getElementById("RegionalDirector").innerHTML = populate.getRegionalDirector(data);

    },

    populateRecruitBusinessMix: function (data) {
        document.getElementById("FinancialPlanningFees").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['FinancialPlanningFees']);
        document.getElementById("AssetMgmtFees").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['AssetMgmtFees']);
        document.getElementById("MutualFunds").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['MutualFunds']);
        document.getElementById("ShareClassIssue").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['ShareClassIssue']);
        document.getElementById("VariableAnnuities").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['VariableAnnuities']);
        document.getElementById("VariableAnnuitiesIssues").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['VariableAnnuitiesIssues']);
        document.getElementById("VariableUnvierseLife").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['VariableUnvierseLife']);
        document.getElementById("FixedIndexedAnnuities").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['FixedIndexedAnnuities']);
        document.getElementById("GeneralSecurities").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['GeneralSecurities']);
        document.getElementById("GeneralSecuritiesDescription").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['GeneralSecuritiesDescription']);
        document.getElementById("UITS").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['UITS']);
        document.getElementById("RetirementPlans").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['RetirementPlans']);
        document.getElementById("RetirementAccountsType").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['RetirementAccountsType']);
        document.getElementById("AlternativeInvestments").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['AlternativeInvestments']);
        document.getElementById("AlternativeInvestmentsDescription").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['AlternativeInvestmentsDescription']);
        document.getElementById("FixedInsurance").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['FixedInsurance']);
        document.getElementById("ExpectstouseCIRMGApartners").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['ExpectstouseCIRMGApartners']);
        document.getElementById("InsuranceProductionIssues").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['InsuranceProductionIssues']);
        document.getElementById("OtherBizMix").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['OtherBizMix']);
        document.getElementById("OtherDescription").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['OtherDescription']);
        document.getElementById("Total").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['Total']);
        document.getElementById("PoPIndividual").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['PoPIndividual']);
        document.getElementById("PoPGroup").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['PoPGroup']);
        document.getElementById("BillingCycle").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['BillingCycle']);
        document.getElementById("Solicitor").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['Solicitor']);
        document.getElementById("Testing").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['Testing']);
        document.getElementById("NonProducingAdmin").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['NonProducingAdmin']);
        document.getElementById("PoPonFile").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['PoPonFile']);
        document.getElementById("PoPNotes").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['PoPNotes']);
        document.getElementById("BillingTime").innerText = advisorCardFunctions.isUndefinedOrNull(data['RecruitBusinessMix']['BillingTime']);
    },
    getRegionalDirector(data){
        var RD = advisorCardFunctions.isUndefinedOrNull(data['BusinessInformation']['RegionalDirectorName']);
        if (RD !== '-')
        {
            var firstandlast = RD.split(" ");
            var mailto = "mailto:" + firstandlast[0] + "." + firstandlast[1] + "@cir2.com?subject=Ignite feedback for " + data["Name"]["FirstName"]  + " " + data["Name"]["LastName"] ;
            RD = "<a href=" + '"' + mailto + '"' + ">" + RD + "</a>";
            //RD = "<a href=" + '"mailto:mohamed.samy@cir2.com?subject=IgniteFeedback"' + ">" + RD + "</a>"

        }
        return RD;
    },

    getEmail(emailaddress){
       var EM = advisorCardFunctions.isUndefinedOrNull(emailaddress);
       var email = "";
        if (EM !== '-')
        {

            var mailto = "mailto:" + EM + "?subject=IgniteFeedback" ;
            email = "<a href=" + '"' + mailto + '"' + ">" + emailaddress + "</a>";


        }
        return email;
    },


    populateActiveRepProd:function(data){

        document.getElementById("GrowthYTD").innerText = advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['GDCPercentGrowthYTD']) + "";
        document.getElementById("CurrentYTD").innerText = "$" + advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['GDCCurrentYTD']);
        document.getElementById("PriorYTD").innerText = "$" + advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['GDCPriorYTD']);
        document.getElementById("CurrentQuarter").innerText = "-";
        document.getElementById("AnnualizedCurrentYTD").innerText = "$" + advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['GDCAnnualizedCurrentYTD']);
        document.getElementById("AnnualizedPriorYTD").innerText = "$" + advisorCardFunctions.isUndefinedOrNull(data['IndividualProduction']['GDCAnnualizedPriorYTD']);
        document.getElementById("PendingCompensation").innerText = "-";


    },

    populateBusinessInformation: function(data){

        document.getElementById("Email").innerHTML = populate.getEmail (advisorCardFunctions.isUndefinedOrNull(data['PersonalInformation']['EmailAddress']) );
        document.getElementById("MobilePhone").innerHTML = populate.getphone(advisorCardFunctions.isUndefinedOrNull(data['PersonalInformation']['MobilePhone']) + "");
        document.getElementById("BusinessPhone").innerHTML = populate.getphone(advisorCardFunctions.isUndefinedOrNull(data['PersonalInformation']['BusinessPhone']) + "");
        document.getElementById("HearsayPhone").innerHTML = populate.getphone(advisorCardFunctions.isUndefinedOrNull(data['PersonalInformation']['HearsayPhone']) + "");


    },

    populateNotes: function(notes){
       var note = advisorCardFunctions.isNameUndefinedOrNull(notes);
       var txtnotes = $("#txtNotes") ;
       txtnotes.val("");
        txtnotes.val(note);
       //txtnotes.text(note);
        //$('#txtNotes').val(notes);
       // $('#txtNotes').text(notes);
    },

    getphone(phone){
        phoneval = advisorCardFunctions.isNameUndefinedOrNull(phone);
        return '<a href='+ '"tel:' + phoneval + '">' + phoneval + '</a>';

    },
    getNotes: function (){
        // parameter is primary compemsation code
        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM Adviser WHERE SearchIndex = ?', [searchIndex], function (tx, results) {
                if (results.rows.length > 0) {
                    var result = advisorCardFunctions.isUndefinedOrNull(results.rows.item(0).NOTES);
                    return result;
                }
                else {
                    return "";
                }
            }, null)
        });

    },
    getRDEMail: function(repcode){
        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM Adviser WHERE repcode Like ?', [repcode], function (tx, results) {
                if (results.rows.length > 0) {
                    var jsonResult = JSON.parse(results.rows.item(0).DATA);
                    return advisorCardFunctions.isUndefinedOrNull(jsonResult[0]["PersonalInformation"]["EmailAddress"]);
                }
                else {
                    return "";
                }
            }, null)
        });
    },
    CAAPBadge: function (data){
        if (data['IndividualProduction']['isCAAPRep'] === "1")
        {
            $("#isCaap").show();
        }
        else
        {
            $("#isCaap").hide();
        }

    },
    getclubmembership: function (data){

            if (data['IndividualProduction']['isLeadersClub'] === "1")
            {
                $("#isLeader").show();
            }
            else
            {
                $("#isLeader").hide();
            }
            if (data['IndividualProduction']['isPremierClub'] === "1")
            {
                $("#isPremier").show();
            }
            else
            {
                $("#isPremier").hide();
            }

             if (data['IndividualProduction']['IsSignatureClub'] === "1")
            {
                $("#isSigniture").show();
            }
            else
             {
                 $("#isSigniture").hide();
             }




    },

    displayDiv: function (divname) {
        document.getElementById(divname).setAttribute("visible", "false");

    },
    getdata: function (data) {
        if ((data === "") || (data === undefined)) {
            return "-";
        }
        else {
            return data;
        }
    }
};

var advisorCardFunctions = {

    hexToBase64: function (hexstring) {
        hexstring = hexstring.substring(2);
        return btoa(hexstring.match(/\w{2}/g).map(function (a) {
            return String.fromCharCode(parseInt(a, 16));
        }).join(""));
    },


    saveNotes: function () {
        var repcode = advisorCardFunctions.isNameUndefinedOrNull($("#repcode").text());
        var notes = advisorCardFunctions.isNameUndefinedOrNull($("#txtNotes").val());
    db.transaction(function(tx) {
        tx.executeSql('update adviser set NOTES=? where SearchIndex=?', [notes,searchIndex] , function(transaction, result) {
            console.log(result);
            console.info('Record Updated Successfully!');

        }, function(transaction, error) {
            console.log(error);
        });
        }, transError, transSuccess);
},
    isUndefinedOrNull: function (data) {
        if ((data === "") || (data === undefined) || (data === null)) {
            return "-";
        }
        else {
            return data;
        }
    },

    isNameUndefinedOrNull: function (data) {
        if ((data === "") || (data === undefined)) {
            return "";
        }
        else {
            return data;
        }
    },

};

// Function for selecting dropdown 

function dropDownMenu() {


    $("#ActiveRepProductionOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').hide();
        $('.selector-res-4').hide();
        $('.selector-res-5').hide();
        $('.selector-res-6').show();
        $('.selector-res-7').hide();
        $('.selector-res-8').hide();
        $('.selector-res-9').hide();
        $('.selector-dropdown').text('Active Rep Production');
        var btnsave = document.getElementById("btnSaveNotes");
        btnsave.innerText = "save";
    });

    $("#BusinessAddressOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').show();
        $('.selector-res-3').hide();
        $('.selector-res-4').hide();
        $('.selector-res-5').hide();
        $('.selector-res-6').hide();
        $('.selector-res-7').hide();
        $('.selector-res-8').hide();
        $('.selector-res-9').hide();
        $('.selector-dropdown').text('Business Address');
        var btnsave = document.getElementById("btnSaveNotes");
        btnsave.innerText = "save";
    });

    $("#BranchInformationOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').show();
        $('.selector-res-4').hide();
        $('.selector-res-5').hide();
        $('.selector-res-6').hide();
        $('.selector-res-7').hide();
        $('.selector-res-8').hide();
        $('.selector-res-9').hide();
        $('.selector-dropdown').text('Branch Information');
        var btnsave = document.getElementById("btnSaveNotes");
        btnsave.innerText = "save";
    });

    $("#SupervisionOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').hide();
        $('.selector-res-4').show();
        $('.selector-res-5').hide();
        $('.selector-res-6').hide();
        $('.selector-res-7').hide();
        $('.selector-res-8').hide();
        $('.selector-res-9').hide();
        $('.selector-dropdown').text('Supervision');
        var btnsave = document.getElementById("btnSaveNotes");
        btnsave.innerText = "save";
    });

    $("#ClearingPartnerOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').hide();
        $('.selector-res-4').hide();
        $('.selector-res-5').show();
        $('.selector-res-6').hide();
        $('.selector-res-7').hide();
        $('.selector-res-8').hide();
        $('.selector-res-9').hide();
        $('.selector-dropdown').text('Clearing Partner');
        var btnsave = document.getElementById("btnSaveNotes");
        btnsave.innerText = "save";
    });

    $("#BusinessInformationOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').hide();
        $('.selector-res-4').hide();
        $('.selector-res-5').hide();
        $('.selector-res-6').hide();
        $('.selector-res-7').show();
        $('.selector-res-8').hide();
        $('.selector-res-9').hide();
        $('.selector-dropdown').text('Business Information');
        var btnsave = document.getElementById("btnSaveNotes");
        btnsave.innerText = "save";
    });

    //notes
    $("#NotesOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').hide();
        $('.selector-res-4').hide();
        $('.selector-res-5').hide();
        $('.selector-res-6').hide();
        $('.selector-res-7').hide();
        $('.selector-res-8').hide();
        $('.selector-res-9').show();


        $('.selector-dropdown').text('Notes');
    });

    $("#RecruitBusinessMixOption").click(function () {
        $('.selector-res-1').hide();
        $('.selector-res-2').hide();
        $('.selector-res-3').hide();
        $('.selector-res-4').hide();
        $('.selector-res-5').hide();
        $('.selector-res-6').hide();
        $('.selector-res-7').hide();
        $('.selector-res-8').show();
        $('.selector-res-9').hide();


        $('.selector-dropdown').text('Recruit Business Mix');
    });
}









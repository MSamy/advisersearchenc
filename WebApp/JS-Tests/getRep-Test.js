﻿let json_result;
{
    json_result = `{
                        "CRMContactId": "825ca3af-c466-4d51-9bdd-7d442be2e6a9",
                        "PrimaryCompensationCode": "VYM",
                        "CRMContactImage": "",
                            "Name": {
                                "FirstName": "Quyn",
                                "MiddleName": "A",
                                "LastName": "Eaton",
                                "Prefix": "",
                                "Suffix": "",
                                "PreferredName": "Alden"
                            },
                            "BusinessInformation": {
                                "CRMPrimaryBranchId": "ca9559ae-903b-4f2d-a1bf-f944a7b66ade",
                                "PrimaryBranchName": "-",
                                "PrimaryBranchNumber": "FM3",
                                "BranchManagerName": "Molly A Chang",
                                "BranchManagerID": "94125a34-04ea-45f5-be6f-d4848afdff73",
                                "BranchManagerPrimaryCompensationCode": "VUV",
                                "OSJId": "8291fb35-e294-46d1-b714-d305d07fe882",
                                "OSJName": "Molly A Chang",
                                "OSJPrimaryCompensationCode": "-",
                                "RegionalDirectorID": "75eaef4a-2090-4505-b5df-7d55b215a5f5",
                                "RegionalDirectorName": "Megan Loftin"
                            },
                            "IndividualProduction": {
                                "PershingAccountCount": 103,
                                "FCCSAccountCount": 0,
                                "OtherAccountCount": 475,
                                "IsCAAPRep": false,
                                "ISLeadersClub": false,
                                "IsPremierClub": false,
                                "ISSignatureClub": false,
                                "AdvisorSince": "10/3/2016",
                                "GDCCurrentYTD": "$183,678.25",
                                "GDCPriorYTD": "$137,752.94",
                                "GDCAnnualizedCurrentYTD": "$377,514.22",
                                "GDCAnnualizedPriorYTD": "$304,528.82",
                                "GDCPercentGrowthYTD": "33.33"
                               }
                        }`;
}

//------------advisorCardFunctions--------------------

describe("[Test] - parseStringToIntAndConvertToThousands", function () {
    var result;
    it("Should take the string '$1,000.00' and convert it to '1'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,000.00");
        expect(result).toEqual(1);
    });
    it("Should take the string '$1,001.00' and convert it to '1'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,001.00");
        expect(result).toEqual(1);
    });
    it("Should take the string '$1,999.99' and convert it to '1'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,999.99");
        expect(result).toEqual(1);
    });
    it("Should take the string '$20,100.00' and convert it to '20'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$20,100.00");
        expect(result).toEqual(20);
    });
    //No Special characters such as '$', ',', and '.'
    it("Should take the string '1000' and convert it to '1'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,000.00");
        expect(result).toEqual(1);
    });
    it("Should take the string '$1001' and convert it to '1'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,001.00");
        expect(result).toEqual(1);
    });
    it("Should take the string '1999' and convert it to '1'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,999.99");
        expect(result).toEqual(1);
    });
    it("Should take the string '20100' and convert it to '20'", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("$20,100.00");
        expect(result).toEqual(20);
    });
    //Try and parse a string with unchecked characters
    it("Should take the string '€20,100.00' and return '0' because '€' isnt defined.", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("€20,100.00");
        expect(result).toEqual(0);
    });
    it("Should take the string 'abcdefg' and return '0' because '€' isnt defined.", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("€20,100.00");
        expect(result).toEqual(0);
    });
    it("Should take the string '' and return '0' because '€' isnt defined.", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("€20,100.00");
        expect(result).toEqual(0);
    });
    it("Should take the string ' ' and return '0' because '€' isnt defined.", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("€20,100.00");
        expect(result).toEqual(0);
    });
    //Verify data type of returned value.
    it("The retun type of parseStringToIntAndConvertToThousands should be of type 'Number'.", () => {
        result = advisorCardFunctions.parseStringToIntAndConvertToThousands("20,100.00");
        expect(result).toEqual(jasmine.any(Number));
    });
});

describe("[Spy]  - parseStringToIntAndConvertToThousands", function () {
    var spy;
    beforeEach(() => {
        spy = spyOn(advisorCardFunctions, 'parseStringToIntAndConvertToThousands');
        advisorCardFunctions.parseStringToIntAndConvertToThousands("$1,000.00");
    });
    it("Test that parseStringToIntAndConvertToThousands function was called.", () => {
        expect(advisorCardFunctions.parseStringToIntAndConvertToThousands).toHaveBeenCalled();
    });
    it("Test that parseStringToIntAndConvertToThousands function was called with parameter '$1,000.00'.", () => {
        expect(advisorCardFunctions.parseStringToIntAndConvertToThousands).toHaveBeenCalledWith("$1,000.00");
    });
});

describe("[Test] - checkAdvisorClubStatus", function () {
    var json_obj, result;

    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    //Check return value of different club combinations
    it("Check advisor club status of a rep with no membership to any club. Should return 0.", () => {
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(0);
    });
    it("Check advisor club status of a rep with membership to the Leaders Club. Should return 1.", () => {
        json_obj['IndividualProduction']['ISLeadersClub'] = true;
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(1);
    });
    it("Check advisor club status of a rep with membership to the Premier Club. Should return 2.", () => {
        json_obj['IndividualProduction']['IsPremierClub'] = true;
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(2);
    });
    it("Check advisor club status of a rep with membership to the Signature Club. Should return 3.", () => {
        json_obj['IndividualProduction']['ISSignatureClub'] = true;
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(3);
    });
    it("Check advisor club status of a rep with membership to the Leaders Club AND Signature Club. Should return 3.", () => {
        json_obj['IndividualProduction']['IsLeadersClub'] = true;
        json_obj['IndividualProduction']['ISSignatureClub'] = true;
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(3);
    });
    it("Check advisor club status of a rep with membership to the Leaders Club AND Premier Club. Should return 2.", () => {
        json_obj['IndividualProduction']['IsLeadersClub'] = true;
        json_obj['IndividualProduction']['IsPremierClub'] = true;
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(2);
    });
    it("Check advisor club status of a rep with membership to the Premier Club AND Signature Club. Should return 3.", () => {
        json_obj['IndividualProduction']['IsPremierClub'] = true;
        json_obj['IndividualProduction']['ISSignatureClub'] = true;
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(3);
    });
    //Verify data type of returned value.
    it("The retun type of checkAdvisorClubStatus should be of type 'Number'.", () => {
        result = advisorCardFunctions.checkAdvisorClubStatus(json_obj);
        expect(result).toEqual(jasmine.any(Number));
    });
});

describe("[Spy]  - checkAdvisorClubStatus", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'checkAdvisorClubStatus');
        advisorCardFunctions.checkAdvisorClubStatus(json_obj);
    });
    it("Test that checkAdvisorClubStatus function was called.", () => {
        expect(advisorCardFunctions.checkAdvisorClubStatus).toHaveBeenCalled();
    });
    it("Test that checkAdvisorClubStatus function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.checkAdvisorClubStatus).toHaveBeenCalledWith(json_obj);
    });
});

describe("Test   - isUndefinedOrNull", function () {
    var result
    it("Should take the string 'Test' and return 'Test'", () => {
        result = advisorCardFunctions.isUndefinedOrNull("Test");
        expect(result).toEqual("Test");
    });
    it("Should take the string '' and return '-'", () => {
        result = advisorCardFunctions.isUndefinedOrNull("");
        expect(result).toEqual("-");
    });
    it("Should take the string UNDEFINED and return '-'", () => {
        result = advisorCardFunctions.isUndefinedOrNull(undefined);
        expect(result).toEqual("-");
    });
    it("Should take the number 100 and return 100", () => {
        result = advisorCardFunctions.isUndefinedOrNull(100);
        expect(result).toEqual(100);
    });
    it("Should take no input and return '-'", () => {
        result = advisorCardFunctions.isUndefinedOrNull();
        expect(result).toEqual("-");
    });
    //Verify data type of returned value.
    it("The retun type of isUndefinedOrNull when supplied with a String return valie should be of type 'String'.", () => {
        result = advisorCardFunctions.isUndefinedOrNull("Test");                        
        expect(result).toEqual(jasmine.any(String));                   
    });                                                                
    it("The retun type of isUndefinedOrNull when supplied with a Number return value should be of type 'Number'.", () => {
        result = advisorCardFunctions.isUndefinedOrNull(100);                            
        expect(result).toEqual(jasmine.any(Number));                    
    });                                                                 
    it("The retun type of isUndefinedOrNull when supplied with a Number return value should be of type 'Number'.", () => {
        result = advisorCardFunctions.isUndefinedOrNull(10.10);                                   
        expect(result).toEqual(jasmine.any(Number));                             
    });                                                                          
    it("The retun type of isUndefinedOrNull when supplied with no Input return value should be of type 'String'.", () => {
        result = advisorCardFunctions.isUndefinedOrNull();
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - isUndefinedOrNull", function () {
    var spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'isUndefinedOrNull');
        advisorCardFunctions.isUndefinedOrNull("TEST");
    });
    it("Test that isUndefinedOrNull function was called.", () => {
        expect(advisorCardFunctions.isUndefinedOrNull).toHaveBeenCalled();
    });
    it("Test that isUndefinedOrNull function was called with parameter 'TEST'.", () => {
        expect(advisorCardFunctions.isUndefinedOrNull).toHaveBeenCalledWith("TEST");
    });
});

describe("[Test] - isNameUndefinedOrNull", function () {
    var result;
    it("Should take the string 'Test' and return 'Test'", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull("Test");
        expect(result).toEqual("Test");
    });
    it("Should take the string '' and return '-'", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull("");
        expect(result).toEqual("");
    });
    it("Should take the string UNDEFINED and return '-'", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull(undefined);
        expect(result).toEqual("");
    });
    it("Should take the number 100 and return 100", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull(100);
        expect(result).toEqual(100);
    });
    it("Should take no input and return '-'", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull();
        expect(result).toEqual("");
    });
    //Verify data type of returned value.
    it("The retun type of isNameUndefinedOrNull when supplied with a String return valie should be of type 'String'.", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull("Test");
        expect(result).toEqual(jasmine.any(String));
    });
    it("The retun type of isNameUndefinedOrNull when supplied with a Number return value should be of type 'Number'.", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull(100);
        expect(result).toEqual(jasmine.any(Number));
    });
    it("The retun type of isNameUndefinedOrNull when supplied with a Number return value should be of type 'Number'.", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull(10.10);
        expect(result).toEqual(jasmine.any(Number));
    });
    it("The retun type of isNameUndefinedOrNull when supplied with no Input return value should be of type 'String'.", () => {
        result = advisorCardFunctions.isNameUndefinedOrNull();
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - isNameUndefinedOrNull", function () {
    var spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'isNameUndefinedOrNull');
        advisorCardFunctions.isNameUndefinedOrNull("TEST");
    });
    it("Test that isNameUndefinedOrNull function was called.", () => {
        expect(advisorCardFunctions.isNameUndefinedOrNull).toHaveBeenCalled();
    });
    it("Test that isNameUndefinedOrNull function was called with parameter 'TEST'.", () => {
        expect(advisorCardFunctions.isNameUndefinedOrNull).toHaveBeenCalledWith("TEST");
    });
});
              
describe("[Test] - createTableData", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    })
    it("Takes the last name [STRING] from the JSON Response and turns it into an HTML div", () => {
        result = advisorCardFunctions.createTableData(json_obj['Name']['LastName']);
        expect(result).toEqual("<div class='col-6 col-md-3 col-lg-3' style='font-weight: bold;'>" + json_obj['Name']['LastName'] + "</div>");
    });
    it("Takes an undefined column from the JSON Response and turns it into an HTML div", () => {
        result = advisorCardFunctions.createTableData(json_obj['Name']['Wow']);
        expect(result).toEqual("<div class='col-6 col-md-3 col-lg-3' style='font-weight: bold;'>" + advisorCardFunctions.isUndefinedOrNull(json_obj['Name']['Wow']) + "</div>");
    });
    it("Takes the Account Count [INT] from the JSON Response and turns it into an HTML div", () => {
        result = advisorCardFunctions.createTableData(json_obj['IndividualProduction']['OtherAccountCount']);
        expect(result).toEqual("<div class='col-6 col-md-3 col-lg-3' style='font-weight: bold;'>" + advisorCardFunctions.isUndefinedOrNull(json_obj['IndividualProduction']['OtherAccountCount']) + "</div>");
    });
    it("Takes the Leader Club [BOOL] from the JSON Response and turns it into an HTML div", () => {
        result = advisorCardFunctions.createTableData(json_obj['IndividualProduction']['ISLeadersClub']);
        expect(result).toEqual("<div class='col-6 col-md-3 col-lg-3' style='font-weight: bold;'>" + advisorCardFunctions.isUndefinedOrNull(json_obj['IndividualProduction']['ISLeadersClub']) + "</div>");
    });
    it("Takes the RegionalDirectorID [GUID] from the JSON Response and turns it into an HTML div", () => {
        result = advisorCardFunctions.createTableData(json_obj['BusinessInformation']['RegionalDirectorID']);
        expect(result).toEqual("<div class='col-6 col-md-3 col-lg-3' style='font-weight: bold;'>" + advisorCardFunctions.isUndefinedOrNull(json_obj['BusinessInformation']['RegionalDirectorID']) + "</div>");
    });
    //Verify that createTableData returns a String
    it("Check return type of createTableData", () => {
        result = advisorCardFunctions.createTableData(json_obj['Name']['LastName']);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - createTableData", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'createTableData');
        advisorCardFunctions.createTableData(json_obj['Name']['LastName']);
    });
    it("Test that createTableData function was called.", () => {
        expect(advisorCardFunctions.createTableData).toHaveBeenCalled();
    });
    it("Test that createTableData function was called with parameter 'json_obj['Name']['LastName']'.", () => {
        expect(advisorCardFunctions.createTableData).toHaveBeenCalledWith(json_obj['Name']['LastName']);
    });
});
               
describe("[Test] - buildConcession2015Table", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });
    
    it("Verifies that buildConcession2015Table returns a String", () => {
        result = advisorCardFunctions.buildConcession2015Table(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildConcession2015Table", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildConcession2015Table');
        advisorCardFunctions.buildConcession2015Table(json_obj);
    });
    it("Test that buildConcession2015Table function was called.", () => {
        expect(advisorCardFunctions.buildConcession2015Table).toHaveBeenCalled();
    });
    it("Test that buildConcession2015Table function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildConcession2015Table).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - buildConcession2016Table", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildConcession2016Table returns a String", () => {
        result = advisorCardFunctions.buildConcession2016Table(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildConcession2016Table", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildConcession2016Table');
        advisorCardFunctions.buildConcession2016Table(json_obj);
    });
    it("Test that buildConcession2016Table function was called.", () => {
        expect(advisorCardFunctions.buildConcession2016Table).toHaveBeenCalled();
    });
    it("Test that buildConcession2016Table function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildConcession2016Table).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - buildActiveRepProductionTable", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildActiveRepProductionTable returns a String", () => {
        result = advisorCardFunctions.buildActiveRepProductionTable(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildActiveRepProductionTable", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildActiveRepProductionTable');
        advisorCardFunctions.buildActiveRepProductionTable(json_obj);
    });
    it("Test that buildActiveRepProductionTable function was called.", () => {
        expect(advisorCardFunctions.buildActiveRepProductionTable).toHaveBeenCalled();
    });
    it("Test that buildActiveRepProductionTable function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildActiveRepProductionTable).toHaveBeenCalledWith(json_obj);
    });
});
              
describe("[Test] - buildClearingPartnerTable", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildClearingPartnerTable returns a String", () => {
        result = advisorCardFunctions.buildClearingPartnerTable(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildClearingPartnerTable", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildClearingPartnerTable');
        advisorCardFunctions.buildClearingPartnerTable(json_obj);
    });
    it("Test that buildClearingPartnerTable function was called.", () => {
        expect(advisorCardFunctions.buildClearingPartnerTable).toHaveBeenCalled();
    });
    it("Test that buildClearingPartnerTable function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildClearingPartnerTable).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - buildSupervisionTable", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildSupervisionTable returns a String", () => {
        result = advisorCardFunctions.buildSupervisionTable(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildSupervisionTable", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildSupervisionTable');
        advisorCardFunctions.buildSupervisionTable(json_obj);
    });
    it("Test that buildSupervisionTable function was called.", () => {
        expect(advisorCardFunctions.buildSupervisionTable).toHaveBeenCalled();
    });
    it("Test that buildSupervisionTable function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildSupervisionTable).toHaveBeenCalledWith(json_obj);
    });
});

describe("[Test] - buildBranchInformationTable", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildBranchInformationTable returns a String", () => {
        result = advisorCardFunctions.buildBranchInformationTable(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildBranchInformationTable", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildBranchInformationTable');
        advisorCardFunctions.buildBranchInformationTable(json_obj);
    });
    it("Test that buildBranchInformationTable function was called.", () => {
        expect(advisorCardFunctions.buildBranchInformationTable).toHaveBeenCalled();
    });
    it("Test that buildSupervisionTable function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildBranchInformationTable).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - buildBusinessAddressTable", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildBusinessAddressTable returns a String", () => {
        result = advisorCardFunctions.buildBusinessAddressTable(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildBusinessAddressTable", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildBusinessAddressTable');
        advisorCardFunctions.buildBusinessAddressTable(json_obj);
    });
    it("Test that buildBusinessAddressTable function was called.", () => {
        expect(advisorCardFunctions.buildBusinessAddressTable).toHaveBeenCalled();
    });
    it("Test that buildBusinessAddressTable function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildBusinessAddressTable).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - buildAddressTable", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildAddressTable returns a String", () => {
        result = advisorCardFunctions.buildAddressTable(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildAddressTable", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildAddressTable');
        advisorCardFunctions.buildAddressTable(json_obj);
    });
    it("Test that buildAddressTable function was called.", () => {
        expect(advisorCardFunctions.buildAddressTable).toHaveBeenCalled();
    });
    it("Test that buildAddressTable function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildAddressTable).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - buildAdvisorCard", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that buildAdvisorCard returns a String", () => {
        result = advisorCardFunctions.buildAdvisorCard(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - buildAdvisorCard", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'buildAdvisorCard');
        advisorCardFunctions.buildAdvisorCard(json_obj);
    });
    it("Test that buildAdvisorCard function was called.", () => {
        expect(advisorCardFunctions.buildAdvisorCard).toHaveBeenCalled();
    });
    it("Test that buildAdvisorCard function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.buildAdvisorCard).toHaveBeenCalledWith(json_obj);
    });
});
              
describe("[Test] - joinAdvisorTables", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Verifies that joinAdvisorTables returns a String", () => {
        result = advisorCardFunctions.joinAdvisorTables(json_obj);
        expect(result).toEqual(jasmine.any(String));
    });
});

describe("[Spy]  - joinAdvisorTables", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'joinAdvisorTables');
        advisorCardFunctions.joinAdvisorTables(json_obj);
    });
    it("Test that joinAdvisorTables function was called.", () => {
        expect(advisorCardFunctions.joinAdvisorTables).toHaveBeenCalled();
    });
    it("Test that joinAdvisorTables function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.joinAdvisorTables).toHaveBeenCalledWith(json_obj);
    });
});
               
describe("[Test] - checkIfKeyExistsInJSON", function () {
    var json_obj, result;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
    });

    it("Check if the key Street exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "Street");
        expect(result).toEqual(false);
    });
    it("Check if the key ISSignatureClub exists in the json object. Should return true.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "ISSignatureClub");
        expect(result).toEqual(true);
    });
    it("Check if the key RegionalDirectorName exists in the json object. Should return true.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "RegionalDirectorName");
        expect(result).toEqual(true);
    });
    it("Check if the key FirstName exists in the json object. Should return true.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "FirstName");
        expect(result).toEqual(true);
    });
    it("Check if the key PercentGrowth2015 exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "PercentGrowth2015");
        expect(result).toEqual(false);
    });
    it("Check if the key GDCPercentGrowthYTD exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "GDCPercentGrowthYTD");
        expect(result).toEqual(true);
    });
    it("Check if the key '' exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "");
        expect(result).toEqual(false);
    });
    it("Check if the key NULL exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, null);
        expect(result).toEqual(false);
    });
    it("Check if the key NULL exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, null);
        expect(result).toEqual(false);
    });
    it("Check if the key MiddleName exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "MiddleName");
        expect(result).toEqual(true);
    });
    it("Check if the key PrimaryCompensationCode exists in the json object. Should return true.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, null);
        expect(result).toEqual(false);
    });
    it("Check if the key UNDEFINED exists in the json object. Should return false.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, undefined);
        expect(result).toEqual(false);
    });
    it("Verify that checkIfKeyPairExists returns a BOOL.", () => {
        result = advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "ISSignatureClub");
        expect(result).toEqual(jasmine.any(Boolean));
    });
});

describe("[Spy]  - checkIfKeyExistsInJSON", function () {
    var json_obj, spy;
    beforeEach(() => {
        json_obj = JSON.parse(json_result);
        spy = spyOn(advisorCardFunctions, 'checkIfKeyExistsInJSON');
        advisorCardFunctions.checkIfKeyExistsInJSON(json_obj, "MiddleName");
    });
    it("Test that checkIfKeyExistsInJSON function was called.", () => {
        expect(advisorCardFunctions.checkIfKeyExistsInJSON).toHaveBeenCalled();
    });
    it("Test that checkIfKeyExistsInJSON function was called with parameter 'json_obj'.", () => {
        expect(advisorCardFunctions.checkIfKeyExistsInJSON).toHaveBeenCalledWith(json_obj, "MiddleName");
    });
});

//------------advisorCardAnimations--------------------
               
describe("[Spy]  - longClickExitCard", function () {
    var spy;
    beforeEach(() => {
        spy = spyOn(advisorCardAnimations, 'longClickExitCard');
        advisorCardAnimations.longClickExitCard("TEST");
    });
    it("Test that longClickExitCard function was called.", () => {
        expect(advisorCardAnimations.longClickExitCard).toHaveBeenCalled();
    });
    it("Test that longClickExitCard function was called with parameter 'TEST'.", () => {
        expect(advisorCardAnimations.longClickExitCard).toHaveBeenCalledWith("TEST");
    });
});
               
describe("[Spy]  - slideTable", function () {
    var spy;
    beforeEach(() => {
        spy = spyOn(advisorCardAnimations, 'slideTable');
        advisorCardAnimations.slideTable("TEST");
    });
    it("Test that slideTable function was called.", () => {
        expect(advisorCardAnimations.slideTable).toHaveBeenCalled();
    });
    it("Test that slideTable function was called with parameter 'TEST'.", () => {
        expect(advisorCardAnimations.slideTable).toHaveBeenCalledWith("TEST");
    });
});


